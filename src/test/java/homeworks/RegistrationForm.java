package homeworks;

import io.github.bonigarcia.wdm.WebDriverManager;
import io.github.bonigarcia.wdm.config.DriverManagerType;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class RegistrationForm {

    WebDriver driver;

    @BeforeMethod
    public void setup() {
        WebDriverManager.getInstance(DriverManagerType.CHROME).setup();
        driver = new ChromeDriver();
        driver.get("http://86.121.249.149:4999/stubs/auth.html");
        WebElement tabSelection = driver.findElement(By.id("register-tab"));
        tabSelection.click();
    }

    @AfterMethod
    public void closeInstance() {
        driver.close();
    }

    @DataProvider(name = "register")
    public Iterator<Object[]> form() {
        Collection<Object[]> dp = new ArrayList<>();
        dp.add(new String[]{"", "", "", "", "", "", "Invalid input. Please enter between 2 and 35 letters", "Invalid input. Please enter between 2 and 35 letters", "Invalid input. Please enter a valid email address", "Invalid input. Please enter between 4 and 35 letters or numbers", "Invalid input. Please use a minimum of 8 characters", "Please confirm your password"});
        dp.add(new String[]{"A", "BC", "siit@gmail.com", "C45C", "12345678", "12345678", "Invalid input. Please enter between 2 and 35 letters", "", "", "", "", ""});
        dp.add(new String[]{"AB", "B", "siit@gmail.com", "C52C", "12345678", "12345678", "", "Invalid input. Please enter between 2 and 35 letters", "", "", "", ""});
        dp.add(new String[]{"AB", "CD", "siit@gmail.com", "CC1", "12345678", "12345678", "", "", "", "Invalid input. Please enter between 4 and 35 letters or numbers", "", ""});
        dp.add(new String[]{"AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", "BBBBBB", "siit@gmail.com", "123ABCF7890X", "12345678", "12345678", "Invalid input. Please enter between 2 and 35 letters", "", "", "", "", ""});
        dp.add(new String[]{"AAAAAAAAAAA", "BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB", "siit@gmail.com", "123ABCF7890X", "12345678", "12345678", "", "Invalid input. Please enter between 2 and 35 letters", "", "", "", ""});
        dp.add(new String[]{"AAAAA", "BBBBB", "siit@gmail.com", "123ABCF7890XH77IJ490LNRJXVFJSUTRDO36", "12345678", "12345678", "", "", "", "Invalid input. Please enter between 4 and 35 letters or numbers", "", ""});
        dp.add(new String[]{"Two-Fnames", "BBBBB", "siit@gmail.com", "123ABCF7890XH77UTRDO3", "12345678", "12345678", "Invalid input. Please enter between 2 and 35 letters", "", "", "", "", ""});
        dp.add(new String[]{"Name", "Two-Snames", "siit@gmail.com", "123ABCF7890XH77UTRDO3", "12345678", "12345678", "", "Invalid input. Please enter between 2 and 35 letters", "", "", "", ""});
        dp.add(new String[]{"Name", "Two Snames", "siit@gmail.com", "123ABCF7890XH77UTRDO3", "12345678", "12345678", "", "Invalid input. Please enter between 2 and 35 letters", "", "", "", ""});
        dp.add(new String[]{"Name", "LastName", "siit@gmail.com", "User With Space", "12345678", "12345678", "", "", "", "Invalid input. Please enter between 4 and 35 letters or numbers", "", ""});
        dp.add(new String[]{"Name", "LastName", "siit@gmail.com", "User-With&Spec!@l-Characters", "12345678", "12345678", "", "", "", "Invalid input. Please enter between 4 and 35 letters or numbers", "", ""});
        dp.add(new String[]{"36490AG", "BBKTL", "siit@gmail.com", "123g", "12345678", "12345678", "Invalid input. Please enter between 2 and 35 letters", "", "", "", "", ""});
        dp.add(new String[]{"NAME", "12XG37", "siit@gmail.com", "123g", "12345678", "12345678", "", "Invalid input. Please enter between 2 and 35 letters", "", "", "", ""});
        dp.add(new String[]{"ab", "cd", "siit@com", "1234", "12345678", "12345678", "", "", "Invalid input. Please enter a valid email address", "", "", ""});
        dp.add(new String[]{"ab", "cd", "siit.com", "1234", "12X45678", "12X45678", "", "", "Invalid input. Please enter a valid email address", "", "", ""});
        dp.add(new String[]{"ab", "cd", "siit@gmail.com", "1A34", "[1e34@-7&8*1)", "[1e34@-7&8*1)", "", "", "", "", "", ""});
        dp.add(new String[]{"ab", "cd", "siit@gmail.com", "1A34", "aaaaaa7", "aaaaaa7", "", "", "", "", "Invalid input. Please use a minimum of 8 characters", "Please confirm your password"});
        dp.add(new String[]{"ab", "cd", "siit@gmail.com", "1234", "12345678", "123456789", "", "", "", "", "", "Please confirm your password"});
        dp.add(new String[]{"ab", "cd", "siit@gmail.com", "1234", "12345678", "12345679", "", "", "", "", "", "Please confirm your password"});
        return dp.iterator();
    }

    @Test(dataProvider = "register")
    public void negativeTestData(String firstN, String lastN, String email, String userN, String pass, String confPass, String firstNErr, String lastNErr, String emailErr, String userNErr, String passErr, String confPassErr) {

        WebElement firstNInput = driver.findElement(By.id("inputFirstName"));
        WebElement lastNInput = driver.findElement(By.id("inputLastName"));
        WebElement emailInput = driver.findElement(By.id("inputEmail"));
        WebElement userNInput = driver.findElement(By.id("inputUsername"));
        WebElement passInput = driver.findElement(By.id("inputPassword"));
        WebElement confPassInput = driver.findElement(By.id("inputPassword2"));
        WebElement submitButton = driver.findElement(By.id("register-submit"));

        firstNInput.clear();
        firstNInput.sendKeys(firstN);
        lastNInput.clear();
        lastNInput.sendKeys(lastN);
        emailInput.clear();
        emailInput.sendKeys(email);
        userNInput.clear();
        userNInput.sendKeys(userN);
        passInput.clear();
        passInput.sendKeys(pass);
        confPassInput.clear();
        confPassInput.sendKeys(confPass);
        submitButton.submit();

        WebElement errFirstN = driver.findElement(By.xpath("//*[@id=\"registration_form\"]/div[3]/div/div[2]"));
        WebElement errLastN = driver.findElement(By.xpath("//*[@id=\"registration_form\"]/div[4]/div/div[2]"));
        WebElement errEmail = driver.findElement(By.xpath("//*[@id=\"registration_form\"]/div[5]/div[1]/div[2]"));
        WebElement errUserN = driver.findElement(By.xpath("//*[@id=\"registration_form\"]/div[6]/div/div[2]"));
        WebElement errPass = driver.findElement(By.xpath("//*[@id=\"registration_form\"]/div[7]/div/div[2]"));
        WebElement errConfPass = driver.findElement(By.xpath("//*[@id=\"registration_form\"]/div[8]/div/div[2]"));

        Assert.assertEquals(errFirstN.getText(), firstNErr);
        Assert.assertEquals(errLastN.getText(), lastNErr);
        Assert.assertEquals(errEmail.getText(), emailErr);
        Assert.assertEquals(errUserN.getText(), userNErr);
        Assert.assertEquals(errPass.getText(), passErr);
        Assert.assertEquals(errConfPass.getText(), confPassErr);
    }
}
