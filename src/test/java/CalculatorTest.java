import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import ro.siit.curs7.Calculator;

public class CalculatorTest {

    static Calculator c;

    @BeforeClass
    public static void beforeTest(){
        c = new Calculator();
    }

    @Test
    public void testSum01() {

        //System.out.println(c.compute(2, 7, "+"));
        Assert.assertEquals(9, c.compute(2, 7, "+"), 0);
    }

    @Test
    public void testSum02() {

        Assert.assertEquals(-13482, c.compute(2164, -15646, "+"), 0);
    }

    @Test
    public void testSum03() {

        Assert.assertEquals(15810, c.compute(164, 15646, "+"), 0);
    }

    @Test
    public void testDiff01() {

        Assert.assertEquals(4, c.compute(164, 160, "-"), 0);
    }

    @Test
    public void testProduct01() {
        Assert.assertEquals(300, c.compute(30, 10, "*"), 0);
    }

    @Test(expected = IllegalArgumentException.class) //asteapta exceptia
    public void testUnsupp01() {
        Assert.assertEquals(300, c.compute(30, 10, "x"), 0);
    }

    @Test
    public void testDiv01() {
        Assert.assertEquals(2, c.compute(10, 5, "/"), 0);
    }

    @Test
    public void testDiv02() {
        Assert.assertEquals(3.33, c.compute(10, 3, "/"), 0.01);
    }

    @Test(expected = IllegalArgumentException.class) //correct way to test if a exception is expected
    public void testDiv03() {
        Assert.assertEquals(0, c.compute(10, 0, "/"), 0);
    }

    //sau (dar e incorect)

    @Test
    public void testDiv04() {
        try {
            Assert.assertEquals(0, c.compute(10, 0, "/"), 0);
        }
        catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
        }
    }

    // ------------------------------My tests -------------------------------

    @Test
    public void sum1() {
        Assert.assertEquals(59, c.compute(39,20,"+"),0);
    }

    @Test
    public void sum2() {
        Assert.assertEquals(-60, c.compute(30,-90,"+"),0);
    }

    @Test
    public void dif1() {
        Assert.assertEquals(-30,c.compute(0, 30, "-"),0);
    }

    @Test
    public void  dif2() {
        Assert.assertEquals(45, c.compute(45,0,"-"), 0);
    }

    @Test
    public void product1() {
        Assert.assertEquals(4, c.compute(2, 2, "*"), 0);
    }

    @Test
    public void product2() {
        Assert.assertEquals(67, c.compute(32, 2,"*"), 3);
    }

    @Test
    public void div1() {
        Assert.assertEquals(45, c.compute(90,1,"/"),45);
    }

    @Test(expected = IllegalArgumentException.class)
    public void div2() {
        Assert.assertEquals(5,c.compute(50, 10, "?"), 0);
    }

    @Test
    public void div3() {
        Assert.assertEquals(3, c.compute(9, 3, "/"), 0);
    }

    @Test
    public void sqrt1() {
        Assert.assertEquals(3, c.compute(9, 1, "SQRT"), 0);
    }

    @Test
    public void sqrt2() {
        Assert.assertEquals(1.71,c.compute(7, 0, "SQRT"), 1);
    }

    //--------------------------End of my tests ---------------------------

    @Test
    public void testSqrt01() {
        Assert.assertEquals(1.4142, c.compute(2, 0, "SQRT"), 0.001);
    }

}
