package ro.siit.curs4.homework;

public class Library {
    public static void main(String[] args) {
        Author a1 = new Author("Jon Doe", "jondoe@gmail.com");
        Book b1 = new Book("of the year", 1990, a1, 23);

        System.out.println("Book " + b1.getName() + " " + "(" + b1.getPrice() + " " + "RON)" + ", by " + a1.getName() + ", published in " + b1.getYear());
    }
}