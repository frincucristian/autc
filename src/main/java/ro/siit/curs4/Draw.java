package ro.siit.curs4;

public class Draw {
    public static void main(String[] args) {
        drawFullShape(Integer.parseInt(args[0]), Integer.parseInt(args[1]));
        System.out.println();
        drawShapeOutline(Integer.parseInt(args[0]), Integer.parseInt(args[1]));
        System.out.println();
        drawShapeCorners(Integer.parseInt(args[0]), Integer.parseInt(args[1]));
    }

    private static void drawShapeCorners(int weight, int height) {
        for (int j = 0; j < height; j++ ) {
            for (int i = 0; i < weight; i++) {
                if (j == 0 || j == height - 1) {
                    if (i == 0 || i == weight - 1) {
                        System.out.print("*");
                    }
                    else {
                        System.out.print(" ");
                    }
                }
                else {
                    System.out.print(" ");
                }
            }
            System.out.println();
        }
    }

    private static void drawShapeOutline(int weight, int height) {
        for (int j = 0; j < height; j++ ) {
            for (int i = 0; i < weight; i++) {
                if (j == 0 || j == height - 1) {
                    System.out.print("*");
                }
                else {
                    if (i == 0 || i == weight - 1) {
                        System.out.print("*");
                    }
                    else {
                        System.out.print(" ");
                    }
                }
            }
            System.out.println();
        }
    }

    public static void drawFullShape(int weight, int height) {
        for (int j = 0; j < height; j++ ) {
            for (int i = 0; i < weight; i++) {
                System.out.print("*");
            }
            System.out.println();
        }
    }
}