package ro.siit.curs3.homework;

    public class LeapYear {
        public static void main(String[] args) {
            int a = Integer.parseInt(args[0]);
            System.out.println(a);

            if ((a % 4 == 0 && a % 100 != 0) || (a % 400 != 0 && a % 100 == 0)){
                System.out.println("On this year February had 29 days, this was a leap year.");
            }
            else {
                System.out.println("This was not a leap year because february had 28 days.");
            }
        }
    }
