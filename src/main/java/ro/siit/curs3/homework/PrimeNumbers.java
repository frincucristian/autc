package ro.siit.curs3.homework;

public class PrimeNumbers {

    public static void main(String[] args) {
        int i;
        int n = 1000000;
        for (i = 2; i < n; i++) {
            boolean prime = true;
            for (int v = 2; v < i; v++) {
                if (i % v == 0) {
                    prime = false;
                    break;
                }
            }
            if (prime) {
                System.out.println(i);
            }
        }
    }
}
