package ro.siit.curs3.homework;

public class SumOfNumbers {
    public static void main(String[] args) {
        int max = 100, i = 1, sum = 0;
        while (i <= max) {
            sum = i + sum;
            i = i + 1;
        }
        System.out.println("Sum = " + sum);
    }
}
