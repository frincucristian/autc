package ro.siit.curs6.homework;

public class Build {

    private String buildingName;

    public Build(String buildingName) {

        this.buildingName = buildingName;
    }

    public String getBuildingName() {
        return buildingName;
    }

    public void setBuildingName(String buildingName) {
        this.buildingName = buildingName;
    }

}
