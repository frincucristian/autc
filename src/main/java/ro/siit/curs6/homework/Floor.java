package ro.siit.curs6.homework;

public class Floor {

    private String conferenceRoom;
    private String office;
    private String kitchen;
    private String toilet;
    private int conferenceNr;
    private int officeNr;
    private int kitchenNr;
    private int toiletNr;

    public Floor(String conferenceRoom, String office, String kitchen, String toilet, int conferenceNr, int officeNr, int kitchenNr, int toiletNr) {
        this.conferenceRoom = conferenceRoom;
        this.office = office;
        this.kitchen = kitchen;
        this.toilet = toilet;
        this.conferenceNr = conferenceNr;
        this.officeNr = officeNr;
        this.kitchenNr = kitchenNr;
        this.toiletNr = toiletNr;
    }

    public Floor(String conferenceRoom, String toilet, int conferenceNr, int toiletNr) {
        this.conferenceRoom = conferenceRoom;
        this.conferenceNr = conferenceNr;
        this.toilet = toilet;
        this.toiletNr = toiletNr;

    }

    public String getConferenceRoom() {
        return conferenceRoom;
    }

    public void setConferenceRoom(String conferenceRoom) {
        this.conferenceRoom = conferenceRoom;
    }

    public String getOffice() {
        return office;
    }

    public void setOffice(String office) {
        this.office = office;
    }

    public String getKitchen() {
        return kitchen;
    }

    public void setKitchen(String kitchen) {
        this.kitchen = kitchen;
    }

    public String getToilet() {
        return toilet;
    }

    public void setToilet(String toilet) {
        this.toilet = toilet;
    }

    public int getConferenceNr() {
        return conferenceNr;
    }

    public void setConferenceNr(int conferenceNr) {
        this.conferenceNr = conferenceNr;
    }

    public int getOfficeNr() {
        return officeNr;
    }

    public void setOfficeNr(int officeNr) {
        this.officeNr = officeNr;
    }

    public int getKitchenNr() {
        return kitchenNr;
    }

    public void setKitchenNr(int kitchenNr) {
        this.kitchenNr = kitchenNr;
    }

    public int getToiletNr() {
        return toiletNr;
    }

    public void setToiletNr(int toiletNr) {
        this.toiletNr = toiletNr;
    }

}