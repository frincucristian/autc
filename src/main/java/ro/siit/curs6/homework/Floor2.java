package ro.siit.curs6.homework;

public class Floor2 extends Floor{
    public Floor2(String conferenceRoom, String office, String kitchen, String toilet, int conferenceNr, int officeNr, int kitchenNr, int toiletNr) {
        super(conferenceRoom, office, kitchen, toilet, conferenceNr, officeNr, kitchenNr, toiletNr);
    }
}
