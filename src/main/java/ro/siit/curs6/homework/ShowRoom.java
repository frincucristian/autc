package ro.siit.curs6.homework;

public class ShowRoom {
    public static void main(String[] args) {
        Build building = new Build("SIIT");
        Floor1 f1 = new Floor1("conference rooms", "office space", "kitchen", "toilets", 3, 1, 1, 2);
        Floor2 f2 = new Floor2("conference rooms", "office spaces", "kitchen", "toilets", 4, 2, 1, 2);
        Floor3 f3 = new Floor3("conference rooms", "toilets", 6, 2);

        System.out.println("Building " + building.getBuildingName() + " -> " + "Floor 1: " + f1.getOfficeNr() + " " + f1.getOffice() + ", " + f1.getToiletNr() + " " + f1.getToilet() + ", " + f1.getKitchenNr() + " " + f1.getKitchen() + ", " + f1.getConferenceNr() + " " + f1.getConferenceRoom());
        System.out.println("                 " + "Floor 2: " + f2.getOfficeNr() + " " + f2.getOffice() + ", " + f2.getToiletNr() + " " + f2.getToilet() + ", " + f2.getKitchenNr() + " " + f2.getKitchen() + ", " + f2.getConferenceNr() + " " + f2.getConferenceRoom());
        System.out.println("                 " + "Floor 3: " + f3.getToiletNr() + " " + f3.getToilet() + ", " + f3.getConferenceNr() + " " + f3.getConferenceRoom());
    }
}
