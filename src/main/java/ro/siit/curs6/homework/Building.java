package ro.siit.curs6.homework;

import java.util.*;

public class Building {

    public static void displayList(List list) {
        for (Object lst : list) {
            System.out.println(lst.toString());
        }
    }

    public static void Floors() {
        List etaj = new ArrayList();
        etaj.add("Floor 1");
        etaj.add("Floor 2");
        etaj.add("Floor 3");
        displayList(etaj);
    }

    public static void Rooms() {
        List camera = new ArrayList();
        camera.add("office spaces");
        camera.add("toilets");
        camera.add("kitchens");
        camera.add("conference rooms");

        displayList(camera);
    }

    public static void displayMap(Map hashMap) {
        List keys = new ArrayList<>(hashMap.keySet());
        Collections.sort(keys);
        for (Object key : keys) {
//          key -> values
            System.out.println(key + " : " + hashMap.get(key));
        }
    }

    public static void showHashMap() {
        Map<String, String> hashMap = new HashMap<>();
        hashMap.put("Floor 1", "office spaces");
        hashMap.put("Floor 2", "toilets");
        hashMap.put("Floor 3", "conference rooms");

        displayMap(hashMap);
    }

    public static void main(String[] args) {
        Floors();
        Rooms();
        showHashMap();
    }
}